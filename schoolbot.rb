require 'discordrb'

ADD_EMOJI = '✌️'
DONE_EMOJI = '✅'
NUM_NEEDED = 2
CHANNEL_NAME = "classes"
CATEGORY_NAME = "other"

# Class representing a server that the bot is in.
# Handles all the inner behavior of the bot.
class Server
    attr_reader :role_channel

    # Store a list of servers the bot is in
    @@servers = {}

    def self.servers
        @@servers
    end

    # Find a channel by name (or nil)
    # @param name [String]
    # @return [Discordrb::Channel]
    def find_channel(name)
        @server.channels.find do |channel|
            channel.name == name
        end
    end

    # Find a role by name (or nil)
    # @param name [String]
    # @return [Discordrb::Role]
    def find_role(name)
        @server.roles.find do |role|
            role.name == name
        end
    end

    # Modify channel permissions to allow only the specified role to see it
    # @param channel [Discordrb::Channel]
    # @param role [Discordrb::Role]
    def modify_channel_permissions!(channel, role)
        @logger.info("Adding permissions to channel #{channel.id}")
        perms = Discordrb::Permissions.new [:read_messages, :send_messages]
        channel.define_overwrite(role, perms)
        channel.define_overwrite(channel.server.everyone_role, 0, perms)
    end

    # Create a channel with given name and role for permissions.
    # @param name [String]
    # @param role [Discordrb::Role]
    # @return [Discordrb::Channel]
    def create_channel!(name, role)
        category = find_channel(CATEGORY_NAME)
        if category.nil? then
            @logger.warn("Could not find category named {}", CATEGORY_NAME)
            return nil
        end
        @logger.info("Creating channel with name #{name}}")
        channel = @server.create_channel(name, parent: category)
        if channel.nil? then
            @logger.warn("Failed to create channel")
            return nil
        end
        modify_channel_permissions!(channel, role)
        channel
    end

    # Create a role with the given name.
    # @param name [String]
    def create_role!(name)
        @logger.info("Creating role with name #{name}")
        @server.create_role(name: name, permissions: Discordrb::Permissions.new)
    end

    # Looks for a channel with CHANNEL_NAME, and does some other setup stuff
    # @param server [Discordrb::Server]
    def initialize(server)
        @server = server
        @role_channel = find_channel(CHANNEL_NAME)
        @logger = Logger.new(STDOUT)
        @logger.level = Logger::DEBUG
        raise ArgumentError, "Could not find channel with name #{CHANNEL_NAME}" if @role_channel.nil?

        @@servers[server] = self
    end

    # If message in channel does not match the correct format, delete it.
    # - If the channel exists but not role, 
    #   immediately create a role and modify the channel permissions and give author the role.
    # - If the role exists but not the channel, immediately create the channel and give the author the role.
    # - If both exist, delete the message.
    # - If neither exist, do the normal vote process.
    # @param message [Discordrb::Message]
    def process_message(message)
        content = message.content.downcase
        unless content =~ /^[a-z]{4}[0-9]{3}[a-z]?$/
            @logger.info('Message does not match')
            message.delete if message.author.id != 213118265633800192
            return
        end
        @logger.info("Processing message #{content}")
        channel = find_channel(content)
        role = find_role(content)
        if role.nil? && channel.nil? then
            message.create_reaction(ADD_EMOJI)
        elsif !(role.nil? || channel.nil?) then
            message.delete
        else
            message.create_reaction(ADD_EMOJI)
            message.create_reaction(DONE_EMOJI)

            if role.nil? then
                role = create_role!(content)
                modify_channel_permissions!(channel, role)
            else
                create_channel!(content, role)
            end
            message.author.add_role(role)
        end
    end

    # If react is the correct emoji
    # - If the bot has not done the things and there are not enough reacts, do nothing.
    # - If there are enough reacts, create a new role, a new channel, and add all users who reacted to the role.
    # - If the bot has done the things, add the user to the role.
    # @param message [Discordrb::Message]
    # @param user [Discordrb::User]
    # @param emoji [String]
    def process_add_reaction(message, user, emoji)
        return unless emoji == ADD_EMOJI

        content = message.content
        if message.my_reactions.none? { |reaction| reaction.name == DONE_EMOJI } then
            # has not passed yet
            @logger.info("Processing reaction on message #{message.id}")
            num_reactions = message.reacted_with(ADD_EMOJI).size
            if num_reactions >= NUM_NEEDED then
                @logger.info("Completing add of #{content}")
                role_to_give = find_role(content)
                if role_to_give.nil? then
                    role_to_give = create_role!(content)
                end
                if find_channel(content).nil? then
                    create_channel!(message, role_to_give)
                end
                message.react(DONE_EMOJI)
                message.reacted_with(ADD_EMOJI).each do |reacted|
                    mem = reacted.on(@server)
                    if mem.nil? then
                        @logger.warn("Reaction user is not a member")
                    else
                        mem.add_role(role_to_give)
                    end
                end
            end
        else
            # has passed
            role_to_give = find_role(content)
            if role_to_give.nil? then
                @logger.warn("Could not find role #{messageContent} to assign")
            else
                @logger.info("Giving #{user.id} role #{content}")
                mem = user.on(@server)
                if mem.nil? then
                    @logger.warn("Reaction user is not a member")
                else
                    mem.add_role(role_to_give)
                end
            end
        end
    end

    # If user removes the correct emoji from a message in the right channel,
    # AND the bot has already reacted done to it,
    # remove the user's role.
    # @param message [Discordrb::Message]
    # @param user [Discordrb::User]
    # @param emoji [String]
    def process_remove_reaction(message, user, emoji)
        return unless emoji == ADD_EMOJI
        content = message.content
        if message.my_reactions.any? { |reaction| reaction.name == DONE_EMOJI } then
            # has passed
            role_to_take = find_role(content)
            if role_to_take.nil? then
                @logger.warn("Could not find role #{content} to remove")
                return
            end
            @logger.info("Removing role for user #{user.id}")
            mem = user.on(@server)
            if mem.nil? then
                @logger.warn("Reaction user is not a member")
            else
                mem.remove_role(role_to_take)
            end
        end
    end
end

if ARGV.empty?
    puts "Must provide bot token as argument"
    exit
end

bot = Discordrb::Bot.new token: ARGV[0]

def process_server(server)
    begin
        Server.new(server)
    rescue ArgumentError
        puts "Tried to join server #{server.id}, could not find channel with name #{CHANNEL_NAME}"
    end
end

def process_event(event)
    server_obj = Server.servers[event.server]
    unless server_obj.nil? then
        if event.message.channel == server_obj.role_channel then
            yield server_obj, event
        end
    end
end

bot.ready do |event|
    event.bot.servers.each_value do |server|
        process_server(server)
    end
end

bot.server_create do |event|
    process_server(event.server)
end

bot.message do |event|
    process_event(event) do |server_obj, valid_event|
        puts "Processing message #{valid_event.content}"
        server_obj.process_message(valid_event.message)
    end
end

bot.reaction_add do |event|
    process_event(event) do |server_obj, valid_event|
        puts "Processing add reaction by #{valid_event.user.id}"
        server_obj.process_add_reaction(valid_event.message, valid_event.user, valid_event.emoji.name)
    end
end

bot.reaction_remove do |event|
    process_event(event) do |server_obj, valid_event|
        puts "Processing remove reaction by #{valid_event.user.id}"
        server_obj.process_remove_reaction(valid_event.message, valid_event.user, valid_event.emoji.name)
    end
end

bot.run
